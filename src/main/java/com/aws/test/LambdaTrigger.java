package com.aws.test;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SNSEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import software.amazon.awssdk.auth.credentials.EnvironmentVariableCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.sns.SnsClient;
import software.amazon.awssdk.services.sns.model.PublishRequest;


public class LambdaTrigger implements RequestHandler<SNSEvent, Object> {
    private String message;
    private JSONObject newMessage;
    private JSONObject objMesssage;
    private int id;
    private Logger logger = LogManager.getLogger(Logger.class.getName());
    public Object handleRequest(SNSEvent event, Context context){
        message = event.getRecords().get(0).getSNS().getMessage();
        objMesssage = new JSONObject(message);
        try{
            id = objMesssage.getInt("id");
            newMessage = new JSONObject();
            newMessage.put("timeStamp",event.getRecords().get(0).getSNS().getTimestamp().toString());
            newMessage.put("description","Test2 Payload Fernando Java-Lambda\n");
            newMessage.put("id",id);
            logger.info("message :" + newMessage.toString());
            sendToTopic(newMessage.toString());
        }
        catch (Exception e){
            logger.error("Problem in the Message Format : " + e);
        }
        return null;
    }

    public void sendToTopic(String message) {
        SnsClient snsClient = SnsClient.builder()
                .region(Region.US_EAST_2)
                .credentialsProvider(EnvironmentVariableCredentialsProvider.create())
                .build();
        PublishRequest publishRequest = PublishRequest.builder().topicArn(System.getenv("ArnTopic")).message(message).build();
        snsClient.publish(publishRequest);
        snsClient.close();
    }
}